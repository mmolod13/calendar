import React from "react";
import "./InputCounter.scss";
import { ICounter } from "./../../interfaces/InputCounter";

import plusIcon from "@assets/image/me-plus.svg";
import minusIcon from "@assets/image/me-minus.svg";

export const InputCounter: React.FC<ICounter> = ({
  value = 0,
  changeValue,
  minValue = 0,
  maxValue = 100,
  step = 1,
  showIncrementIcon = true,
}) => {
  return (
    <div className="m-counter">
      <div className="m-counter__wrapper">
        {showIncrementIcon && (
          <img
            src={minusIcon}
            alt="-"
            onClick={(event) => changeCounter(event, value - step)}
          />
        )}
        <input
          className="m-counter__input"
          type="number"
          step={step}
          value={value}
          onChange={(event) => handleInput(event)}
        />
        {showIncrementIcon && (
          <img
            src={plusIcon}
            alt="+"
            onClick={(event) => changeCounter(event, value + step)}
          />
        )}
      </div>
    </div>
  );

  function handleInput(event: React.ChangeEvent<HTMLInputElement>): void {
    changeValue(setRightValue(Number(event.target.value), minValue, maxValue));
  }

  function changeCounter(
    event: React.MouseEvent<HTMLImageElement>,
    value: number
  ): void {
    changeValue(setRightValue(value, minValue, maxValue));
    event.preventDefault();
  }

  function setRightValue(
    value: number,
    minValue: number,
    maxValue: number
  ): number {
    if (Number.isNaN(Number(value))) return 0;
    if (value >= minValue && value <= maxValue) return value;
    if (value < minValue) return minValue;
    return maxValue;
  }
};
