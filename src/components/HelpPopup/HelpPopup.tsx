import React from "react";
import "./HelpPopup.scss";

import greenBall from "@assets/image/me-green-ball.svg";
import redBall from "@assets/image/me-red-ball.svg";
import steps from "@assets/image/me-steps.svg";
import greenStar from "@assets/image/star-green.svg";
import goldStar from "@assets/image/star-gold.svg";
import meOk from "@assets/image/me-ok.svg";
import arrow from "@assets/image/arrow.svg";
import arrowShot from "@assets/image/arrow_shot.svg";

export const HelpPopup: React.FC = () => {
  return (
    <div className="me-help">
      <div className="me-help__title">Explanation</div>
      <div className="me-help__section-title">
        How do I achieve a check mark?
      </div>
      <div className="me-help__section-images">
        <img src={greenBall} alt="green ball" />
        <img src={arrowShot} alt="arrow" className="small" />
        <span>2</span>
        <img src={arrow} alt="arrow" className="small" />
        <img src={meOk} alt="ok" />
      </div>

      <div className="me-help__section-text">
        Get 2 Green counts a day and earn green check mark
      </div>
      <div className="me-help__section-images">
        <img src={steps} alt="steps" />
        <img src={arrowShot} alt="arrow" className="small" />
        <span>1000</span>
        <img src={arrow} alt="arrow" className="small" />
        <img src={meOk} alt="ok" />
      </div>

      <div className="me-help__section-text">
        Get 1000 steps and earn green check mark
      </div>
      <div className="me-help__section-images">
        <img src={redBall} alt="redBall" />
        <img src={arrowShot} alt="arrow" className="small" />
        <span>1</span>
        <img src={arrow} alt="arrow" className="small" />
        (
        <img src={meOk} alt="ok" />)
      </div>
      <div className="me-help__section-text">
        Get 1 Red counts a day and lost green check mark
      </div>

      <div className="me-help__section-title">How do I achive a star?</div>
      <div className="me-help__section-images">
        <img src={meOk} alt="ok" />
        <img src={meOk} alt="ok" />
        <img src={meOk} alt="ok" />
        <img src={arrow} alt="arrow" className="small" />
        <img src={greenStar} alt="greenStar" className="greenStar" />
      </div>
      <div className="me-help__section-text">
        Get 3 green check marks a day - add a green star to your calendar
      </div>

      <div className="me-help__section-title">How do I achive a gold star?</div>
      <div className="me-help__section-images">
        <div>
          <img src={greenStar} alt="greenStar" className="greenStar" />
          <img src={greenStar} alt="greenStar" className="greenStar" />
          <img src={greenStar} alt="greenStar" className="greenStar" />
        </div>
        <img src={arrow} alt="arrow" className="small" />
        <img src={goldStar} alt="goldStar" className={Object.keys({ goldStar })[0]} />
      </div>
      <div className="me-help__section-text">
        Get 3 green stars - add a gold star to your calendar
      </div>
    </div>
  );
};
