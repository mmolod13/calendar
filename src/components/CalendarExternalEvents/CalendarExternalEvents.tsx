import React, { useEffect } from "react";
import { Draggable } from "@fullcalendar/interaction";
import "./CalendarExternalEvents.scss";

import { ICalendarExternalEvents, IEvent } from "./../../interfaces/CalendarExternalEvents";

export const CalendarExternalEvents: React.FC<ICalendarExternalEvents> = ({
  greenStarsCount,
  goldStarsCount,
}) => {
  const draggableEl = React.createRef<HTMLDivElement>();
  const externalEvents: IEvent[] = [
    {
      count: greenStarsCount,
      title: "greenStar",
    },
    {
      count: goldStarsCount,
      title: "goldStar",
    },
  ];

  useEffect(() => {
    if (!draggableEl?.current) {
      throw new Error("The element external-events wasn't found");
    }
    new Draggable(draggableEl.current, {
      itemSelector: ".fc-event",
      eventData: function (eventEl) {
        return {
          title: eventEl.getAttribute("title"),
        };
      },
    });
  }, []);

  return (
    <div ref={draggableEl} id="external-events">
      {externalEvents.map((event) => {
        return (
          <div key={event.title} className="wrapper">
            <div
              className={"fc-event " + event.title}
              title={event.title}
            ></div>
            <div className="event-count">{event.count}</div>
          </div>
        );
      })}
    </div>
  );
};
