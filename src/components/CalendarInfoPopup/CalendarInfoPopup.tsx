import React from "react";
import "./CalendarInfoPopup.scss";

import { ICalendarInfoPopup } from "./../../interfaces/CalendarInfoPopup";
import { getMood } from "./../../utils";

import greenBall from "@assets/image/me-green-ball.svg";
import redBall from "@assets/image/me-red-ball.svg";
import steps from "@assets/image/me-steps.svg";
import happy from "@assets/image/me-emoji-happy.svg";
import neutral from "@assets/image/me-emoji-neutral.svg";
import sad from "@assets/image/me-emoji-sad.svg";

export const CalendarInfoPopup: React.FC<ICalendarInfoPopup> = ({
  currentDateObj,
}) => {
  const mood = getMood(currentDateObj);
  return (
    <div className={"me-popup"}>
      <div className="me-popup__line">
        <img src={greenBall} alt="greenBall" />
        <span>{currentDateObj?.greenBalls}</span>
        natural foods eaten
      </div>
      <div className="me-popup__line">
        <img src={redBall} alt="redBall" />
        <span>{currentDateObj?.redBalls}</span>
        non-natural foods eaten
      </div>
      <div className="me-popup__line">
        <img src={steps} alt="steps" />
        <span>{currentDateObj?.countSteps}</span>
        steps done
      </div>
      <div className="me-popup__line">
        <img src={getModdIcon(mood)} alt="mood" />
        <span>{mood}</span>
        mood
      </div>
    </div>
  );
};

function getModdIcon(mood: string) {
  if (mood === "happy") return happy;
  if (mood === "sad") return sad;
  return neutral;
}