import React from "react";
import "./StatusHealth.scss";
import { IHealth } from "./../../interfaces/StatusHealth";

import happy from "@assets/image/me-emoji-happy.svg";
import neutral from "@assets/image/me-emoji-neutral.svg";
import sad from "@assets/image/me-emoji-sad.svg";

export const StatusHealth: React.FC<IHealth> = ({ status = "choose" }) => {
  const statusArray = [
    [happy, "happy"],
    [neutral, "neutral"],
    [sad, "sad"],
  ]
  return (
    <div className="m-status">
      {statusArray.map((item) => (
        <div
          key={item[1]}
          className={`m-status__line${
            status === item[1] ? " active" : ""
          }`}
        >
          <img src={item[0]} alt="icon" />
        </div>
      ))}
    </div>
  );
};
