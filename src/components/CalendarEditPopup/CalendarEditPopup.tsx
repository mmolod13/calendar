import React from "react";
import "./CalendarEditPopup.scss";

import { InputCounter } from "../InputCounter/InputCounter";
import { StatusHealth } from "../StatusHealth/StatusHealth";
import { getMood } from "../../utils";

import { IDayResult } from "./../../interfaces/Calendar";
import { IPartState } from "./../../interfaces/CalendarEditPopup";

export const CalendarEditPopup: React.FC<{
  handleClose: () => void;
  getResultOnDate: (date: string) => IDayResult;
  changeGlobalState: (result: IDayResult) => void;
  selectedDate: string;
}> = ({ handleClose, selectedDate, changeGlobalState, getResultOnDate }) => {
  const currentData: IDayResult = getResultOnDate(selectedDate);
  const [editResult, setEditResult] = React.useState<IDayResult>(currentData);

  React.useEffect(() => {
    setEditResult((prev: IDayResult) => ({ ...prev, ...currentData, date: selectedDate }));
  }, [selectedDate, currentData]);

  const setPartState = (partialData: IPartState): void =>
    setEditResult((prev: IDayResult) => {
      return {...prev, ...partialData, date: selectedDate };
    });

  const isActiveButClear: boolean =
    currentData.greenBalls !== editResult.greenBalls ||
    currentData.redBalls !== editResult.redBalls ||
    currentData.countSteps !== editResult.countSteps
      ? true
      : false;
      
  const handlerClear = (): void =>
    setEditResult((prev: IDayResult) => ({
      ...prev,
      ...currentData,
      date: selectedDate
    }));

  const handleEditData = (): void => {
    changeGlobalState(editResult);
    handleClose();
  };
  return (
    <div className="m-editor">
      <button
        onClick={handlerClear}
        className="m-editor__clear-button"
        disabled={!isActiveButClear}
      >
        Clear all
      </button>
      <span></span>
      <label>Date</label>
      <div className="m-editor__date">
        <div className="m-editor__date-text">{selectedDate}</div>
      </div>
      <label>Green counts</label>
      <InputCounter
        value={editResult.greenBalls}
        changeValue={(value: number) => setPartState({ greenBalls: value })}
      />
      <label>Red counts</label>
      <InputCounter
        value={editResult.redBalls}
        changeValue={(value: number) => setPartState({ redBalls: value })}
      />
      <label>Steps counts</label>
      <InputCounter
        value={editResult.countSteps}
        changeValue={(value: number) => setPartState({ countSteps: value })}
        showIncrementIcon={false}
        maxValue={100000}
      />
      <label>Your feelings</label>
      <StatusHealth status={getMood(editResult)} />
      <div className="m-editor__control">
        <button className="m-cancel _icon-close-small" onClick={handleClose}>
          Cancel
        </button>
        <button
          className="m-save _icon-ok-small"
          disabled={!isActiveButClear}
          onClick={handleEditData}
        >
          Save
        </button>
      </div>
    </div>
  );
};
