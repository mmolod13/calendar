import React, { useState, useEffect } from "react";
import "./Calendar.scss";
import { setDayData, getDataFromMonth } from "./../../services/calendarService";
import FullCalendar, {
  DateSelectArg,
  EventContentArg,
  DayCellContentArg,
  DatesSetArg,
  EventAddArg,
} from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import interactionPlugin from "@fullcalendar/interaction";

import {
  getCheсkMarksCount,
  generateEventItems,
  getFormatedStrToDate,
  getFormatedDateToStr,
} from "./../../utils";   

import { IDayResult, IEvent } from "./../../interfaces/Calendar";

import { Popup } from "../Popup/Popup";
import { CalendarEditPopup } from "../CalendarEditPopup/CalendarEditPopup";
import { HelpPopup } from "../HelpPopup/HelpPopup";
import { CalendarInfoPopup } from "./../CalendarInfoPopup/CalendarInfoPopup";
import { CalendarExternalEvents } from "./../CalendarExternalEvents/CalendarExternalEvents";

const greenStarsMaxCount = 21;
const goldStarsMaxCount = 7;

const defaultInfoObject = {
  greenBalls: 0,
  redBalls: 0,
  countSteps: 0, 
  greenStars: 0,
  goldStars: 0,
};

export const Calendar: React.FC = () => {
  const [data, setData] = useState<IDayResult[]>([]);
  const [eventsData, setEventsData] = useState<IEvent[]>([]);
  const [greenStarsCount, setGreenStarsCount] = useState<number>(greenStarsMaxCount);
  const [goldStarsCount, setGoldStarsCount] = useState<number>(goldStarsMaxCount);
  const [currentDropDate, setCurrentDropDate] = useState<string>("");
  const [currentClickDate, setCurrentClickDate] = useState<string>("");
  const [selectedDate, setSelectedDate] = useState<string>("");
  const [currentMonthNumber, setCurrentMonthNumber] = useState<number>(new Date().getMonth() + 1);
  const [currentFullYearNumber, setCurrentFullYearNumber] = useState<number>(new Date().getFullYear());
  const [startMonthTime, setStartMonthTime] = useState<number>(0);
  const [endMonthTime, setEndMonthTime] = useState<number>(0);
  const [showModal, setShowModal] = React.useState<number>(0);

  useEffect(() => {
    const resultEventsArray: IEvent[] = data
      .map((item) => {
        if(!item.date) return [];
        return [
          ...generateEventItems(getCheсkMarksCount(item), item.date,"checkMarket"),
          ...generateEventItems(item.greenStars, item.date, "greenStar"),
          ...generateEventItems(item.goldStars, item.date, "goldStar"),
        ];
      })
      .flat();
    setEventsData([...resultEventsArray]);
    updateStarsCount();
  }, [data]);

  React.useEffect(() => {
    getDataFromMonth(
      currentMonthNumber,
      currentFullYearNumber
    ).then((response) => setData(response.data));
  }, [currentMonthNumber, currentFullYearNumber]);

  useEffect(() => {
    document.addEventListener("click", handleGlobalClick, true);
    return () => {
      document.removeEventListener("click", handleGlobalClick, true);
    };
  }, []);

  return (
    <div className="app">
      <CalendarExternalEvents
        greenStarsCount={greenStarsCount}
        goldStarsCount={goldStarsCount}
      />
      <div className="app-main">
        <FullCalendar
          plugins={[dayGridPlugin, interactionPlugin]}
          headerToolbar={{
            left: "prev,title,next",
            center: "",
            right: "help today",
          }}
          dayHeaderFormat={{
            weekday: "long",
          }}
          buttonText= {{
            today: 'Today'
         }}
          customButtons={{
            help: {
              text: "",
              click: function () {
                setShowModal(2);
              },
            },
          }}
          initialView="dayGridMonth"
          showNonCurrentDates={false}
          editable={true}
          droppable={true}
          selectable={true}
          selectMirror={false}
          dayMaxEvents={true}
          datesSet={handleDatesSet}
          dayCellContent={injectCellContent}
          dayCellClassNames={injectCellClassName}
          eventAllow={handleEventAllow}
          drop={handleDateDrop}
          weekends={true}
          events={eventsData}
          eventOrder={handleEventOrder}
          select={handleDateSelect}
          eventContent={renderEventContent}
          eventReceive={handleAddEvents}
        />
        <Popup
          isShow={!!showModal}
          handleClose={hideModal}
          children={
            showModal === 1 ? (
              <CalendarEditPopup
                handleClose={hideModal}
                selectedDate={currentClickDate}
                changeGlobalState={handleChangeGlobalState}
                getResultOnDate={getCurrentInfoObject}
              />
            ) : (
              <HelpPopup />
            )
          }
        />
      </div>
    </div>
  );

  function injectCellClassName(args: DayCellContentArg) {
    return selectedDate === getFormatedDateToStr(args.date) ? "popup-open": "";
  }

  function injectCellContent(args: DayCellContentArg) {
    const daysOdds = Math.ceil(
      (new Date().getTime() - args.date.getTime()) / (1000 * 3600 * 24)
    );
    const isUpdateButton = daysOdds < 4 && daysOdds > 0 ? "show" : "";
    return (
      <div>
        <div className={"day-number"}>{args.dayNumberText}</div>
        <div
          className={"update-button " + isUpdateButton}
          onClick={(event) => {
            event.stopPropagation();
            openUpdatePopup(args.date)
          }}
        >
          Update
        </div>
        <CalendarInfoPopup currentDateObj={getCurrentInfoObject()} />
      </div>
    );
  }

  function renderEventContent(eventContent: EventContentArg) {
    eventContent.isDraggable = false;
    eventContent.isEndResizable = false;
    return <div className={eventContent.event.title}></div>;
  }

  function handleAddEvents(eventInfo: EventAddArg) {
    eventInfo.event.remove();
    const eventType = eventInfo.event._def.title;
    const currentDateObj = data.find((item) => item.date === currentDropDate);
    if (!currentDateObj) return;
    if (eventType === "goldStar" && goldStarsCount >= 1) {
      currentDateObj.greenStars -= 3;
      currentDateObj.goldStars += 1;
      setGoldStarsCount(goldStarsCount - 1);
      setGreenStarsCount(greenStarsCount + 3);
    } else if (eventType === "greenStar" && greenStarsCount >= 1) {
      currentDateObj.greenStars += 1;
      setGreenStarsCount(greenStarsCount - 1);
    }

    setData([...data]);
    setDayData(currentDateObj, "PUT");
  }

  function handleGlobalClick(event: Event) {
    const eventPathArray = event.composedPath();
    if (
      eventPathArray.length > 0 &&
      eventPathArray.some((el:any ) => el.classList?.contains("m-modal"))
    ) {
      return;
    }
   
    setCurrentClickDate("");
    setSelectedDate("");
  }

  function handleChangeGlobalState(obj: IDayResult) {
    let queryMethod = "POST";
    let objToSend = {...obj};
    let currentObjIndex = data.findIndex((item) => item.date === objToSend.date);
    if (currentObjIndex >= 0) {
      queryMethod = "PUT";
      objToSend = data[currentObjIndex] = { ...objToSend, greenStars: 0, goldStars: 0 };
      setData([...data]);
    } else {
      setData([...data, objToSend]);
    }
    setDayData( objToSend, queryMethod);
  }

  function handleDatesSet(dateInfo: DatesSetArg) {
    setCurrentMonthNumber(dateInfo.start.getMonth() + 1);
    setCurrentFullYearNumber(dateInfo.start.getFullYear());

    setStartMonthTime(dateInfo.start.getTime());
    setEndMonthTime(dateInfo.end.getTime());
  }

  function handleEventAllow(dropInfo: any, draggedEvent: any) {
    const eventType = draggedEvent._def.title;
    const cellDate = dropInfo.startStr;
    const cellData = data.find((item) => item.date === cellDate);

    if (!cellData) return false;
    if (
      (eventType === "greenStar" && getCheсkMarksCount(cellData) >= 3) ||
      (eventType === "goldStar" && cellData.greenStars >= 3)
    ) return true;

    return false;
  }

  function handleDateDrop(dropInfo: any) {
    setCurrentDropDate(dropInfo.dateStr);
  }

  function handleDateSelect(selectInfo: DateSelectArg) {
    selectInfo.jsEvent?.stopPropagation();
    setCurrentClickDate(selectInfo.startStr);
    setSelectedDate(selectInfo.startStr);
  }

  function handleEventOrder(): number {
    return 0;
  }

  function hideModal() {
    setShowModal(0);
  };

  function updateStarsCount() {
    const dataForCurrentMonth = data.filter((item) => {
      const itemTime = item.date? getFormatedStrToDate(item.date).getTime() : 0;
      return itemTime >= startMonthTime && itemTime <= endMonthTime;
    });
    if (dataForCurrentMonth.length === 0) {
      setGreenStarsCount(greenStarsMaxCount);
      setGoldStarsCount(goldStarsMaxCount);
      return;
    }
    const greenStarsUsed = dataForCurrentMonth.reduce(
      (prev, cur) => ({ greenStars: prev.greenStars + cur.greenStars }),
      { greenStars: 0 }
    ).greenStars;
    const goldStarsUsed = dataForCurrentMonth.reduce(
      (prev, cur) => ({ goldStars: prev.goldStars + cur.goldStars }),
      { goldStars: 0 }
    ).goldStars;

    setGreenStarsCount(greenStarsMaxCount - greenStarsUsed);
    setGoldStarsCount(goldStarsMaxCount - goldStarsUsed);
  }

  function openUpdatePopup(date: Date) {
    setShowModal(1);
    setCurrentClickDate(getFormatedDateToStr(date));
  }

  function getCurrentInfoObject(date: string = currentClickDate) {
    return data.find((item) => item.date === date) || defaultInfoObject;
  }
};
