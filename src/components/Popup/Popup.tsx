import React from "react";
import "./Popup.scss";

import { IPopup } from "./../../interfaces/Popup";

export const Popup: React.FC<IPopup> = ({ handleClose, isShow, children }) => {
  const toogleShow: string = isShow ? "m-modal show" : "m-modal hide";
  return (
    <div className={toogleShow}>
      <section className="m-modal__main">
        {children}
        <button onClick={handleClose}>&#10005;</button>
      </section>
    </div>
  );
};
