import axios from "axios";

const urlAPI: string = process.env.REACT_APP_API_BASE_URL || "";
const userID: string = process.env.REACT_APP_API_USER_ID || "";

export function getDataFromMonth(month:number, year:number) {
  return axios
    .get(`${urlAPI}/month/${userID}?month=${month}&year=${year}`);
}

export function getDayData(date = "2021-01-01") {
  return axios
    .get(`${urlAPI}/day/${userID}/${date}`)
    .then(response => response.data);
}

export function setDayData(data: any, method: any = "POST") {
  return axios({
    url: urlAPI,
    method,
    data: { personID: userID, ...data },
  });
}
