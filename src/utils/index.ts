import moment from "moment";
import { IDayResult } from "./../interfaces/Calendar";

export function getMood({ greenBalls, redBalls, countSteps }: IDayResult) {
  if (greenBalls === 0 && redBalls === 0 && countSteps === 0) return "choose";
  let coefficient: number =
    (greenBalls + Math.floor(countSteps / 1000)) / (redBalls ? redBalls : 1);
  if (coefficient > 2) return "happy";
  if (coefficient < 1) return "sad";
  return "neutral";
}

export function getCheсkMarksCount({
  greenBalls,
  redBalls,
  countSteps,
  greenStars,
  goldStars,
}: IDayResult) {
  const count =
    Math.floor(greenBalls / 2) +
    Math.floor(countSteps / 1000) -
    redBalls -
    greenStars * 3 -
    goldStars * 9;
  return count < 0 ? 0 : count;
}

export function getFormatedDateToStr(
  date: Date,
  dateFormat = "YYYY-MM-DD"
): string {
  return moment(date).format(dateFormat);
}

export function getFormatedStrToDate(
  str: string,
  dateFormat = "YYYY-MM-DD"
): Date {
  return moment(str, dateFormat).toDate();
}

export function generateEventItems(item: any, date: string, title: string) {
  return (
    (item > 0 &&
      Array(item)
        .fill("")
        .map(() => ({
          title,
          date,
        }))) ||
    []
  );
}
