export interface ICalendarExternalEvents {
  greenStarsCount: number;
  goldStarsCount: number;
}

export interface IEvent {
  count: number;
  title: string;
}