export interface IHealth {
  status: "happy" | "neutral" | "sad" | "choose";
}
