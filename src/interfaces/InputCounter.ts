export interface ICounter {
    value: number;
    changeValue: (n: number) => void;
    minValue?: number;
    maxValue?: number;
    step?: number;
    showIncrementIcon?: boolean;
  }