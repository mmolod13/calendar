export interface IDayData {
  date?: string;
  greenBalls: number;
  redBalls: number;
  countSteps: number;
  greenStars: number;
  goldStars: number;
}

export interface IDayResult extends IDayData {
  personID?: string;
  __v?: number;
  _id?: string;
}

export interface IEvent {
  date: string;
  title: string;
}
