
import { IDayData } from "./Calendar";

export interface ICalendarInfoPopup {
  currentDateObj: IDayData;
}