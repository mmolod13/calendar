export interface IPartState {
  greenBalls?: number;
  redBalls?: number;
  countSteps?: number;
}
