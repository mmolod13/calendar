export interface IPopup {
    handleClose: () => void;
    isShow: boolean;
    children: React.ReactNode;
  }